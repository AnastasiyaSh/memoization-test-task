from datetime import datetime


def get_current_timestamp():
    return datetime.now(tz=None).timestamp()


def fib(n):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fib(n - 1) + fib(n - 2)


def power(x, y):
    return x ** y


def custom_fib_resolver(n):
    return f"{n}-range"


def custom_power_resolver(x, y):
    return f"{x}^{y}"


def info_func():
    print("This is an informative function with no return")


def division(x, y):
    return x/y
