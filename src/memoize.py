from typing import Callable, Union

from src.utils import get_current_timestamp


def memoize(func: Callable, resolver: Union[Callable, str, int, float] = None, timeout: int = 5,
            cache_capacity: int = 100, cache_clearance_timeout: int = 7) -> Callable:
    """
    Main purpose of this function is to store previously calculated values of the wrapped function in the cache

    Parameters that can be passed:
    :param cache_clearance_timeout: number of seconds to store all cached values (clears after timeout is reached)
    :param cache_capacity: number of maximum items in cache (clears after overload)
    :param func: any function that should be memoized
    :param resolver: function that can manipulate on func arguments and create specific key
    :param timeout: number of seconds to hold a value in the cache
    :return: returns the result of memoized function calculation
    """
    cache = {"created_at": get_current_timestamp()}

    def memoized_func(*args):
        current_timestamp = get_current_timestamp()
        key = args[0]

        if get_current_timestamp() - float(cache["created_at"]) > cache_clearance_timeout or len(
                cache) >= cache_capacity:
            # Remember, that cache has one default item - current_timestamp
            print(len(cache))
            cache.clear()
            cache["created_at"] = get_current_timestamp()

        if callable(resolver):
            key = resolver(*args)
        elif resolver:
            key = resolver

        if key not in cache:
            # Result calculating the first time and caching.
            result = {"value": func(*args), "timestamp": current_timestamp}
            cache[key] = result
        elif current_timestamp - cache[key]["timestamp"] > timeout:
            # Timeout exceeded. Result overwriting with the new timestamp.
            result = {"value": func(*args), "timestamp": current_timestamp}
            cache[key] = result
        # Timeout did not exceed, returning the cached value.
        return cache[key]["value"]

    return memoized_func
