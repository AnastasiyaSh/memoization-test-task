import time

import pytest

from src.memoize import memoize
from src.utils import fib, power, info_func, division


def test_memoize_one_arg(memoized_fib):
    # Checking the positive test case of wrapped function working with one argument
    assert memoized_fib(5) == 5


def test_memoize_one_arg_with_resolver(memoized_fib_with_resolver):
    # Checking the positive test case of wrapped function working with one argument and resolver
    assert memoized_fib_with_resolver(5) == 5


def test_memoize_two_args(memoized_power):
    # Checking the positive test case of wrapped function working with several arguments
    assert memoized_power(2, 3) == 8


def test_memoize_two_args_with_resolver(memoized_power_with_resolver):
    # Checking the positive test case of wrapped function working with several arguments and resolver
    assert memoized_power_with_resolver(2, 3) == 8


def test_memoize_two_args_with_resolver_and_timeout(memoized_power_with_resolver_and_timeout):
    # Checking the positive test case of wrapped function working with several arguments and resolver
    assert memoized_power_with_resolver_and_timeout(2, 3) == 8


def test_memoize_call_with_different_arguments(memoized_fib_with_resolver):
    # Checking the negative test case with different arguments and resolvers.
    first_function_call = memoized_fib_with_resolver(5)
    second_function_call = memoized_fib_with_resolver(6)
    assert second_function_call != first_function_call


def test_expiration(memoized_fib_with_resolver):
    """Checking the positive test case of function recalculating the result after the timeout exceeds.
    In this test it is better to show the time of calculating the first and the second result to prove,
    that the function was really calculating twice. I don`t have enough technical background to make that.
    But this test improved coverage from 89% to 100%, I assume it works correctly.
     """
    first_function_call = memoized_fib_with_resolver(5)
    time.sleep(7)
    second_function_call = memoized_fib_with_resolver(5)
    assert second_function_call == first_function_call


def test_memoize_different_arguments_same_resolver():
    # Checking the test case with different arguments, but same resolver key.
    memoized_function_call = memoize(fib, resolver="xyz")
    first_function_call = memoized_function_call(3)
    time.sleep(2)
    second_function_call = memoized_function_call(4)
    assert second_function_call == first_function_call


def test_defaults_check():
    """Checking the test case of comparing the two function calls:
    with no optional arguments written and with default optional arguments.
    """
    t1 = memoize(fib)
    t2 = memoize(fib, resolver=None, timeout=5)
    assert t1(1) == t2(1)


def test_func_argument_with_index_error():
    # Checking the test case with function without argument call
    with pytest.raises(IndexError):
        f1 = memoize(info_func)
        f1()


def test_func_argument_with_type_error():
    # Checking the test case with passing the argument to the function, that not requires argument
    with pytest.raises(TypeError):
        f1 = memoize(info_func())
        f1("some not requires text")


def test_func_arguments_missing_index_error():
    """Checking the test case when  arguments missing.
    Note: function call errors because of impossibility to add key[0] of an empty tuple ()
    """
    with pytest.raises(IndexError):
        f1 = memoize(power)
        f1()


def test_func_one_argument_missing_type_error():
    # Checking the test case when one argument missing
    with pytest.raises(TypeError):
        f1 = memoize(power)
        f1(1)


def test_func_one_argument_extra_type_error():
    # Checking the test case when there are extra arguments
    with pytest.raises(TypeError):
        f1 = memoize(power)
        f1(1, 2, 3)


def test_resolver_with_type_error_dict():
    # Checking the test case with resolver incorrect type
    with pytest.raises(TypeError):
        f1 = memoize(fib, resolver={"a": "asd"})
        f1(1)


def test_resolver_with_type_error_list():
    # Checking the test case with resolver incorrect type
    with pytest.raises(TypeError):
        f1 = memoize(fib, resolver=[1, 3, 4])
        f1(1)


def test_resolver_with_type_error_set():
    # Checking the test case with resolver incorrect type
    with pytest.raises(TypeError):
        f1 = memoize(fib, resolver={"a", "asd", True, 1})
        f1(1)


def test_timeout_with_type_error_dict():
    # Checking the test case with timeout incorrect type
    with pytest.raises(TypeError):
        f1 = memoize(fib, timeout={"a": "asd"})
        f1(1)
        time.sleep(1)
        f1(1)


def test_timeout_with_type_error_tuple():
    # Checking the test case with timeout incorrect type
    with pytest.raises(TypeError):
        f1 = memoize(fib, timeout=("a", "asd"))
        f1(1)
        time.sleep(1)
        f1(1)


def test_timeout_with_type_error_set():
    # Checking the test case with timeout incorrect type
    with pytest.raises(TypeError):
        f1 = memoize(fib, timeout={"a", "asd", True, 1})
        f1(1)
        time.sleep(1)
        f1(1)


def test_func_argument_division_by_zero():
    # Checking the test case with zero division in function result
    with pytest.raises(ZeroDivisionError):
        f1 = memoize(division)
        f1(9, 0)


def test_resolver_func_division_by_zero():
    # Checking the test case with zero division in resolver
    with pytest.raises(ZeroDivisionError):
        f1 = memoize(division, resolver=2/0)
        f1(9, 1)


def test_unexpected_keyword_argument_error():
    # Checking the test case of keyword misspelling
    with pytest.raises(TypeError):
        f1 = memoize(power, reslver=12)
        f1(9, 1)


def test_memoization():
    timeout = 5
    y = 1

    def sum_of_two(x):
        return x + y

    test_func = memoize(sum_of_two, timeout=timeout)
    first_call = test_func(1)
    y = 2
    # the timeout did not expire,
    # so the memoized value is returned (no new function call and so the new y-value is not considered)
    assert first_call == test_func(1)
    time.sleep(5)
    # wait until timeout expired.
    # The timeout expired, so the function is called and the new y-value is used for the calculation.
    assert first_call != test_func(1)


def test_cache_clearance_by_timeout():
    # Checking the test case of cache clearance. Cache is cleared every 7 secs.
    test_func = memoize(power, resolver=12)
    first_call = test_func(4, 5)
    # wait until timeout for cache clearance will expire
    time.sleep(8)
    # The resolver is the same and the timeout for function recalculation was not reached.
    # But as the cache clearance timeout is reached, all data are deleted.
    # Second call will be recalculated.
    second_call = test_func(6, 5)
    assert second_call != first_call


def test_negative_cache_clearance_by_timeout():
    # Checking the test case of cache clearance. Cache is cleared every 7 secs.
    test_func = memoize(power, resolver=12)
    first_call = test_func(4, 5)
    # wait until timeout for cache clearance will expire
    time.sleep(2)
    # As the resolver is the same and the timeout for function recalculation was not reached,
    # function should return saved value. But the cache clearance timeout is reached before.
    second_call = test_func(6, 5)
    assert second_call == first_call


def test_cache_clearance_by_overfill():
    # Checking the test case of cache clearance by overfill (max 99 items)
    # Cache stores 100 values: current_timestamp and 99 results of test_func.
    test_func = memoize(power)
    random_func_call = None
    for i in range(0, 100):
        function_call = test_func(i, 10)
        if i == 4:
            random_func_call = function_call

    last_func_call = test_func(4, 15)
    # The last_function_call has the same resolver, as the first_function_call.
    # As the timeout was not reached (timeout for overwrite is 5, for cache clearance - 7),
    # cache cleared after 100 items, first_function_call with resolver 0 was deleted,
    # last_function_call was recalculated.
    assert random_func_call != last_func_call


def test_negative_cache_clearance_by_overfill():
    # Checking the test case of cache clearance by overfill (max 99 items)
    # Cache stores 100 values: current_timestamp and 99 results of test_func.
    test_func = memoize(power)
    first_func_call = None
    for i in range(0, 50):
        function_call = test_func(i, 10)
        if i == 5:
            first_func_call = function_call

    last_func_call = test_func(5, 15)
    # The last_function_call has the same resolver, as the first_function_call.
    # As the timeout was not reached (timeout for overwrite is 5, for cache clearance - 7),
    # and cache was not overloaded, last_func_call will be called by 0 resolver and return
    # first_func_call result.
    assert first_func_call == last_func_call
