# Memoization test task

The purpose of this test task was to create a memoization function, which implements [memoization pattern](https://en.wikipedia.org/wiki/Memoization).

## Description

`memoize()` function is a decorator of any function, that can be passed into it.
It creates cache for decorated function results and can be used for performance optimization of future calculations. 
In our implementation `memoize()` function can accept one argument - function to be memoized.
Decorated function can accept:
* *args - for function arguments
 
### Resolver concept
Resolver is an optional parameter, that can be passed as a cache key for storing the function execution result.
If the same key is already in the cache, the value of memoized function execution can be taken by the resolver key.
 
Resolver can be passed as value and function call both for development convenience:
 
```python
memoize(func, resolver=resolver_func)
```

Resolver **can not** take unhashable data types: list, dict, set.

### Timeout
Timeout is an optional parameter.
The result of the memoized function becomes invalid after the timeout exceeds. 

Default value of the timeout is _5_ sec.
Timeout can take int values.

### Cache clearance
In order not to overload cache dictionary, cache will be cleared automatically if the clearance timeout reached it max value (_7_ sec),
 or if the cache capacity will reach _99_ items.

## How to run the project
To run the project, you should have installed python on your local machine (version up to 3.7 is preferred).
This project has `requirements.txt` file, so you can simply run 
 
``` 
python -m pip install -r requirements.txt
```

or in some cases

```
python3 -m pip install -r requirements.txt
```
to install needed dependencies.
 
To run all tests use `pytest tests` command in your terminal.

In case you want to check test coverage, please run `coverage run -m pytest` .
To receive coverage report, please run `coverage report`.

Expected result: [screenshot](https://prnt.sc/1xljqh4)
