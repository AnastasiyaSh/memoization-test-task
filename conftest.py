import pytest

from src.memoize import memoize
from src.utils import custom_fib_resolver, custom_power_resolver, fib, power


@pytest.fixture
def memoized_fib():
    return memoize(fib)


@pytest.fixture
def memoized_fib_with_resolver():
    return memoize(fib, resolver=custom_fib_resolver)


@pytest.fixture
def memoized_power():
    return memoize(power)


@pytest.fixture
def memoized_power_with_resolver():
    return memoize(power, resolver=custom_power_resolver)


@pytest.fixture
def memoized_power_with_resolver_and_timeout():
    return memoize(power, resolver=custom_power_resolver, timeout=4)
